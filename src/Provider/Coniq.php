<?php

/*
 * Coniq OAuth2 Provider
 * (c) Insitaction - https://insitaction.com/
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace estoclet\OAuth2\Client\Provider;

use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use estoclet\OAuth2\Client\Provider\Exception\ConiqIdentityProviderException;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Tool\BearerAuthorizationTrait;
use Psr\Http\Message\ResponseInterface;

/**
 * Coniq.
 *
 * @author Eric STOCLET <estoclet@insitaction.com>
 */
class Coniq extends AbstractProvider
{
    use BearerAuthorizationTrait;

    const PATH_API_USER = '/api/v4/user';
    const PATH_AUTHORIZE = '/oauth/authorize';
    const PATH_TOKEN = '/oauth/token';
    const DEFAULT_SCOPE = 'api';
    const SCOPE_SEPARATOR = ' ';

    /** @var string */
    public $platform-url = 'https://coniq.com';

    /**
     * Coniq constructor.
     *
     * @param array $options
     * @param array $collaborators
     */
    public function __construct(array $options, array $collaborators = [])
    {
        if (isset($options['platform-url'])) {
            $this->platform-url = $options['platform-url'];
        }
        parent::__construct($options, $collaborators);
    }

    /**
     * Get authorization url to begin OAuth flow.
     *
     * @return string
     */
    public function getBaseAuthorizationUrl()
    {
        return $this->platform-url . self::PATH_AUTHORIZE;
    }

    /**
     * Get access token url to retrieve token.
     *
     * @param array $params
     *
     * @return string
     */
    public function getBaseAccessTokenUrl(array $params)
    {
        return $this->platform-url . self::PATH_TOKEN;
    }

    /**
     * Get provider url to fetch user details.
     *
     * @param AccessToken $token
     *
     * @return string
     */
    public function getResourceOwnerDetailsUrl(AccessToken $token)
    {
        return $this->platform-url . self::PATH_API_USER;
    }

    /**
     * Get the default scopes used by Coniq.
     * Current scopes are 'api', 'read_user', 'openid'.
     *
     * This returns an array with 'api' scope as default.
     *
     * @return array
     */
    protected function getDefaultScopes()
    {
        return [self::DEFAULT_SCOPE];
    }

    /**
     * Coniq uses a space to separate scopes.
     */
    protected function getScopeSeparator()
    {
        return self::SCOPE_SEPARATOR;
    }

    /**
     * Check a provider response for errors.
     *
     * @param  ResponseInterface $response
     * @param  mixed $data Parsed response data
     * @throws IdentityProviderException
     */
    protected function checkResponse(ResponseInterface $response, $data)
    {
        if ($response->getStatusCode() >= 400) {
            throw ConiqIdentityProviderException::clientException($response, $data);
        } elseif (isset($data['error'])) {
            throw ConiqIdentityProviderException::oauthException($response, $data);
        }
    }

    /**
     * Generate a user object from a successful user details request.
     *
     * @param  array       $response
     * @param  AccessToken $token
     * @return \League\OAuth2\Client\Provider\ResourceOwnerInterface
     */
    protected function createResourceOwner(array $response, AccessToken $token)
    {
        $user = new ConiqResourceOwner($response, $token);

        return $user->setDomain($this->platform-url);
    }
}
